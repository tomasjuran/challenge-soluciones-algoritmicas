#!/usr/bin/env sh

while :
do
  sleep 5
  PGPASSWORD=$POSTGRES_PASSWORD psql -U "$POSTGRES_USER" -h db "$POSTGRES_DB" < update_query.sql
done