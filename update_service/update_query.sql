WITH rnd (rnd) AS (SELECT random() / 20 + 0.98)
UPDATE app_product SET precio_compra = floor(precio_compra * 100 * rnd) / 100, precio_venta_publico = floor(precio_venta_publico * 100 * rnd) / 100, ultima_actualizacion = NOW()
FROM rnd;
