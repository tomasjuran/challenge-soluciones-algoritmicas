__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

from src.client.Client import Client
from src.client.Product import Product
from tests import helpers


def client_factory(product_id: str = None) -> Client:
    if product_id is None:
        product_id = helpers.load_test_dict()["id"]
    return Client(product_id)


def test_non_existent_id_returns_none():
    client = client_factory("None")
    assert client.lineReceived(
        str.encode('{"error": "An error occurred"}')) is None


def test_data_is_received_correctly():
    client = client_factory()
    data = helpers.load_test_json()
    expected: Product = Product().read_from_dict(helpers.load_test_dict())
    product: Product = client.lineReceived(str.encode(data))
    assert product == expected


def test_data_is_read_correctly():
    client = client_factory()
    test_data = helpers.load_test_dict()
    product: Product = client.read_data(test_data)
    expected = Product().read_from_dict(test_data)
    assert product == expected
