__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

from datetime import datetime

from src.client.Product import Product
from tests import helpers


def test_product_reads_dict_correctly():
    product = Product()
    expected = helpers.load_test_dict()
    product.read_from_dict(expected)
    assert product.id == expected["id"]
    assert product.precio_compra == float(expected["precio_compra"])
    assert product.precio_venta_publico == float(expected["precio_venta_publico"])
    assert product.descripcion == expected["descripcion"]
    assert product.ultima_actualizacion == datetime.fromisoformat(
            expected["ultima_actualizacion"])