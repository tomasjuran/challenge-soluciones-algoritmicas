__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

import json


def load_test_json() -> str:
    return '{' \
           '"id":"AC041",' \
           '"precio_compra":392.85,' \
           '"precio_venta_publico":470.00,' \
           '"descripcion":"Atún Al Natural La Campagnola X170g",' \
           '"ultima_actualizacion":"2022-03-10 22:05:02.386-03:00"' \
           '}'


def load_test_dict() -> dict:
    return json.loads(load_test_json())
