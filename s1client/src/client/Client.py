__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

import json
import logging

from twisted.internet import reactor
from twisted.internet.protocol import connectionDone as twistedConnectionDone
from twisted.protocols.basic import LineOnlyReceiver
from twisted.python.failure import Failure as twistedFailure

from src.client.Product import Product


class Client(LineOnlyReceiver):
    """Connects to a server to receive price updates for a product"""
    # Override superclass variables
    delimiter = b"\n"
    MAX_LENGTH = 2 ** 24

    def __init__(self, product_id: str):
        self.product_id = product_id
        self.logger = logging.getLogger("root")
        self.remote = ""

    def connectionMade(self):
        """When the connection is successful, send ID to query"""
        self.remote = f"{self.transport.getPeer().host}:" \
                      f"{self.transport.getPeer().port}"
        self.logger.info(f"Connected to {self.remote}")
        self.sendLine(self.product_id.encode())
        self.logger.info(f"Sent '{self.product_id}' to {self.remote}")

    def connectionLost(self, reason: twistedFailure = twistedConnectionDone):
        if isinstance(reason, twistedConnectionDone.__class__):
            self.logger.info(f"Connection ended with {self.remote}")
        else:
            self.logger.error(f"Connection ended with {self.remote} - "
                              f"Reason: {reason}")
        if reactor.running:
            reactor.stop()

    def lineReceived(self, data: bytes) -> Product:
        self.logger.info(f"Received data from {self.remote}")
        self.logger.debug(data)

        received: dict = json.loads(bytes.decode(data))
        if "error" in received:
            self.logger.error(f"An error ocurred: {received['error']}")
            if self.transport is not None:
                self.transport.loseConnection()
            return None
        return self.read_data(dict(received))

    def read_data(self, data: dict) -> Product:
        """Reads data from a dict and logs the price listing"""
        product: Product = Product().read_from_dict(data)
        self.logger.info(f"Received update: {product}")
        return product
