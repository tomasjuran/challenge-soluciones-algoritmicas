#!/usr/bin/env python3

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

import argparse
import json
import logging
import logging.config

from twisted.internet import reactor
from twisted.internet.protocol import ClientCreator
from twisted.python.failure import Failure as twistedFailure

from src.client.Client import Client

CONFIG_FILENAME = "config.json"
REMOTE_ADDR_KEY = "remote_addr"
REMOTE_PORT_KEY = "remote_port"
LOG_CONFIG_FILE_KEY = "log_config"

logger = logging.getLogger("root")


def main(arguments):
    with open(arguments.config) as f:
        config = json.load(f)
    with open(config[LOG_CONFIG_FILE_KEY]) as f:
        logging.config.dictConfig(json.load(f))

    remote_addr = getattr(arguments, REMOTE_ADDR_KEY, config[REMOTE_ADDR_KEY])
    remote_port = getattr(arguments, REMOTE_PORT_KEY, config[REMOTE_PORT_KEY])

    creator = ClientCreator(reactor, Client, arguments.product_id)
    creator.connectTCP(remote_addr, remote_port) \
        .addErrback(connection_failed,
                    host=remote_addr,
                    port=remote_port)
    print("Press CTRL + C to stop client...")
    reactor.run()


def connection_failed(reason: twistedFailure, host: str, port: int):
    logger.error(f"Connection failed to {host}:{port} - "
                 f"Reason: {reason}")
    reactor.stop()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Connect to a server to receive price updates")
    parser.add_argument("product_id", help="Product ID to query")
    parser.add_argument("-s", "--remote-addr",
                        help="Server address to connect to",
                        default=argparse.SUPPRESS)
    parser.add_argument("-p", "--remote-port", type=int,
                        help="Server port to connect to",
                        default=argparse.SUPPRESS)
    parser.add_argument("-c", "--config",
                        help="Configuration file in json format \
                            (default: 'config.json')",
                        default=CONFIG_FILENAME)
    args = parser.parse_args()
    main(args)
