# Challenge - Soluciones Algorítmicas
## Consignas:
Se requiere implementar 3 servicios:
 - **Servicio 1 (S1):** Debe implementar un cliente socket TCP con S2, para solicitar y
leer actualizaciones de precios. No es necesario que la información recibida sea
persistida pero si se requiere que sea interpretada correctamente.
 - **Servicio 2 (S2):** Debe atender conexiones de S1 y recibir una única solicitud de
información por cada socket abierto al principio de la conexión. Las solicitudes
deben consistir en el ID de producto que será solicitado a S3. La información
recibida del S3 se responderá vía socket al S1 en un formato de texto plano a definir
por el entrevistado.
   - S2 deberá volver a consultar en intervalos regulares de tiempo por el precio
del producto al S3 y enviará la actualización por el mismo socket que originó
la solicitud.
   - Cuando S1 cierra el socket con S2, S2 dejará de solicitar precios del
producto correspondiente al socket cerrado a S3.
 - **Servicio 3 (S3):** Debe implementar un servidor HTTP que responda las solicitudes
de S2 mediante API REST. Cada producto consta de los siguientes campos que
deben figurar en la respuesta a la consulta:
   - Código de producto o ID (texto de longitud variable)
   - Precio de compra (número real)
   - Precio de venta al público (número real)
   - Descripción (texto de longitud variable)
   
    También se debe implementar un mecanismo que actualice periódicamente los
precios de los productos en intervalos regulares de tiempo.

Los servicios deberán poder ejecutarse en procesos y servidores independientes.

## Solución propuesta:

Para el pasaje de mensajes (tanto de S3 a S2 como de S2 a S1) se utiliza un formato *json* simple con clave-valor:

```
{
    "id":"IDPRO1",
    "precio_compra":200.00,
    "precio_venta_publico":300.00,
    "descripcion":"DESCRIPCIÓN DEL PRODUCTO",
    "ultima_actualizacion":"2022-03-10 22:05:02.386-03:00"
}
```

### Frameworks

El stack utilizado se compone por:

 - Python 3.10
 - API REST Django (S3)
 - Framework Twisted (S2, S1)
 - Base de datos Postgres (Actualizador de precios S3)
 - Contenedores Docker, donde se ejecuta cada servicio

### Test run

Para realizar una ejecución de prueba del stack entero, utilizar el comando

`docker-compose up`

desde el directorio raíz. Es posible que inicializar la base de datos tome más de lo requerido, previniendo incluir los datos de testing en la primer ejecución. De ser así, usar `CTRL + C` para detener el stack y luego ejecutar el comando anterior nuevamente. 

Para ejecutar pruebas particulares, se pueden mapear las redes de cada servicio a localhost (con la bandera `--network="host"`), de la siguiente manera:

```
cd s1client
docker build -t client .
docker run -it --rm --network="host" client:latest ./main.py <id_producto> -s localhost -p <puerto s2>
```

El archivo `docker-compose.yml` de S3 cumple esta función, permitiendo acceder desde el navegador del host a la API utilizando la dirección `http://localhost:8000`

### Testing & Linting

Para ejecutar las pruebas y linting, utilizar los scripts de los directorios *scripts* de cada servicio (requiere instalar los paquetes incluidos en los respectivos `requirements.txt`)

`scripts/run_tests_and_linting.sh`

