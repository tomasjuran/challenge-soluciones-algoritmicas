#!/usr/bin/env bash

echo "Running tests..."
pytest -q tests
echo "Running linting checks..."
flake8 src main.py