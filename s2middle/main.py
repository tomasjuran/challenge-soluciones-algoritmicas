#!/usr/bin/env python3

__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

import argparse
import json
import logging
import logging.config

from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ServerEndpoint

from src.middle.MiddleServiceFactory import MiddleServiceFactory
from src.middle.Repository import Repository

CONFIG_FILENAME = "config.json"
SERVICE_PORT_KEY = "service_port"
API_URL_KEY = "api_url"
DELAY_KEY = "delay"
LOG_CONFIG_FILE_KEY = "log_config"

logger = logging.getLogger("root")


def main(arguments):
    with open(arguments.config) as f:
        config = json.load(f)
    with open(config[LOG_CONFIG_FILE_KEY]) as f:
        logging.config.dictConfig(json.load(f))

    service_port = getattr(arguments, SERVICE_PORT_KEY,
                           config[SERVICE_PORT_KEY])
    api_url = getattr(arguments, API_URL_KEY, config[API_URL_KEY])
    api_url = api_url if api_url.endswith("/") else api_url + "/"
    delay = getattr(arguments, DELAY_KEY, config[DELAY_KEY])

    endpoint = TCP4ServerEndpoint(reactor, service_port)
    endpoint.listen(MiddleServiceFactory(Repository(api_url), delay))
    logger.info(f"Service listening on port {service_port}")
    reactor.run()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Price listing service")
    parser.add_argument("-p", "--service-port", type=int,
                        help="Port to open for clients",
                        default=argparse.SUPPRESS)
    parser.add_argument("-a", "--api-url",
                        help="API address to query price listings",
                        default=argparse.SUPPRESS)
    parser.add_argument("-d", "--delay", type=int,
                        help="Delay between client responses",
                        default=argparse.SUPPRESS)
    parser.add_argument("-c", "--config",
                        help="Configuration file in json format \
                            (default: 'config.json')",
                        default=CONFIG_FILENAME)
    args = parser.parse_args()
    main(args)
