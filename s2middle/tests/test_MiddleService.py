__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

import json

from src.middle.MiddleService import MiddleService
from src.middle.MiddleServiceFactory import MiddleServiceFactory
from src.middle.Repository import Repository
from src.middle.RepositoryError import APIUnreachable, \
    IDNotFound
from tests import helpers


class MockRepository(Repository):
    repo = {"AC041": helpers.load_test_json()}
    raiseAPIError = False

    def __init__(self):
        super().__init__("")

    def request(self, product_id: str) -> str:
        if self.raiseAPIError:
            raise APIUnreachable
        if product_id not in self.repo:
            raise IDNotFound
        return self.repo[product_id]


class MockFactory(MiddleServiceFactory):
    def __init__(self):
        super().__init__(MockRepository(), 60)


class MockService(MiddleService):
    def __init__(self):
        super().__init__()
        self.factory = MockFactory()


def test_product_received_correctly():
    service = MockService()
    error = service.query_product("AC041")
    assert error is None
    product = json.loads(service.product)
    assert product["id"] == "AC041"


def test_inexistent_product_handled_correctly():
    service = MockService()
    error = service.query_product("")
    assert error is not None
    assert error == IDNotFound.msg


def test_api_unreachable_handled_correctly():
    factory = MockFactory()
    repository = MockRepository()
    repository.raiseAPIError = True
    factory.repository = repository
    service = MockService()
    service.factory = factory
    error = service.query_product("AC041")
    assert error is not None
    assert error == APIUnreachable.msg
