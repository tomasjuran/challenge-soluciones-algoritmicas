__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

from dataclasses import dataclass
from datetime import datetime


@dataclass
class Product:
    """Product with price updates"""
    id: str = ""
    precio_compra: float = 0.0
    precio_venta_publico: float = 0.0
    descripcion: str = ""
    ultima_actualizacion: datetime = datetime.utcnow()

    def read_from_dict(self, data: dict):
        self.id = data["id"]
        self.precio_compra = float(data["precio_compra"])
        self.precio_venta_publico = float(data["precio_venta_publico"])
        self.descripcion = data["descripcion"]
        self.ultima_actualizacion = datetime.fromisoformat(
            data["ultima_actualizacion"])

        return self

    def __str__(self) -> str:
        return f"id: {self.id} - precio de compra: ${self.precio_compra:.2f}" \
               f" - precio de venta al público: " \
               f"${self.precio_venta_publico:.2f}" \
               f" - última actualización: {self.ultima_actualizacion}"

    def __eq__(self, other) -> bool:
        if self is other:
            return True
        if not isinstance(other, Product):
            return False
        for attribute in vars(self).keys():
            if getattr(self, attribute) != getattr(other, attribute):
                return False
        return True

    def __hash__(self):
        return hash((x for x in vars(self).values()))
