__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

import logging

from twisted.internet.protocol import connectionDone as twistedConnectionDone
from twisted.internet.task import LoopingCall
from twisted.protocols.basic import LineOnlyReceiver
from twisted.python.failure import Failure as twistedFailure

from src.middle.RepositoryError import RepositoryError


class MiddleService(LineOnlyReceiver):
    """Connection to a client

    self.factory holds parameters for all instances,
    such as delay between responses and the URL to connect to, to get updates

    :ivar product: string to send to client
    :ivar connected: flag if still connected to client
    :ivar looping_call: loop to answer client requests periodically
    """
    # Override superclass variables
    delimiter = b"\n"
    MAX_LENGTH = 2 ** 24

    def __init__(self):
        self.logger = logging.getLogger("root")
        self.remote = ""
        self.product: str = ""
        self.connected = False
        self.looping_call: LoopingCall = None

    def connectionMade(self):
        self.remote = f"{self.transport.getPeer().host}:" \
                      f"{self.transport.getPeer().port}"
        self.logger.info(f"Connected to {self.remote}")
        self.connected = True

    def connectionLost(self, reason: twistedFailure = twistedConnectionDone):
        if isinstance(reason, twistedConnectionDone.__class__):
            self.logger.info(f"Connection ended with {self.remote}")
        else:
            self.logger.error(f"Connection ended with {self.remote} - "
                              f"Reason: {reason}")
        self.connected = False

    def lineReceived(self, data: bytes):
        self.logger.info(f"Received request from {self.remote}")
        self.logger.debug(data)

        received: str = bytes.decode(data)
        self.looping_call = LoopingCall(self.send_response, received)
        self.looping_call.start(self.factory.delay)

    def send_response(self, product_id: str):
        error: str = self.query_product(product_id)
        if error is None and self.connected:
            # send response to client
            self.sendLine(self.product.encode())
            self.logger.info(f"Sent response to {self.remote}")
            self.logger.debug(self.product)
        else:
            # break out of loop
            self.looping_call.stop()
            if error is not None:
                # product doesn't exist or can't connect to API
                self.logger.error(
                    f"An error occurred while processing a request from "
                    f"{self.remote} - {error} - Received {product_id}")
            if self.connected:
                # end connection if still connected
                self.sendLine(f'{{"error": "{error} - '
                              f'Received {product_id}"}}'.encode())
                self.transport.loseConnection()

    def query_product(self, product_id: str) -> str | None:
        """Query the API for a price update. Store result in self.product"""
        try:
            response = self.factory.repository.request(product_id)
            # everything ok, update product
            self.product = response
            return None
        except RepositoryError as e:
            # an error occurred, bubble up
            return e.msg
        except Exception as e:
            return e.__str__()
