__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"


class RepositoryError(Exception):
    msg: str = ""


class APIUnreachable(RepositoryError):
    msg = "API unreachable, service unavailable"


class IDNotFound(RepositoryError):
    msg = "Product ID does not exist"
