__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

import logging

import requests

from src.middle.RepositoryError import APIUnreachable, IDNotFound


class Repository:
    """Repository to get the data from"""

    def __init__(self, api_url: str):
        self.api_url = api_url
        self.logger = logging.getLogger("root")

    def request(self, product_id: str) -> str:
        """Request a remote API

        :param product_id: the id of the product to request
        :return: the response (string) if success
        :raises: RepositoryError, on an exception
        """
        response = requests.get(self.api_url + product_id + "/?format=json")
        # ID not found
        if response.status_code == 404:
            raise IDNotFound
        # if the server suffered another error, inform so
        if response.status_code != 200:
            raise APIUnreachable
        # if no error, return response
        return response.text
