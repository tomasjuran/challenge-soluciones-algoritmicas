__author__ = "Tomás Martín Juran"
__version__ = "1.0.0"

from twisted.internet.protocol import ServerFactory

from src.middle.MiddleService import MiddleService
from src.middle.Repository import Repository


class MiddleServiceFactory(ServerFactory):
    protocol = MiddleService

    def __init__(self, repository: Repository, delay: int):
        """Set the repository to ask for data and request delay in seconds"""
        self.repository = repository
        self.delay = delay
