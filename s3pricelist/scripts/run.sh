#!/usr/bin/env sh

sleep 5
python manage.py migrate
python manage.py loaddata test_products.json
python manage.py runserver 0:8000
