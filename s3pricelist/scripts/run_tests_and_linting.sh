#!/usr/bin/env bash

echo "Running tests..."
pytest -q app
echo "Running linting checks..."
flake8 app
