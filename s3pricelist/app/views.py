from rest_framework import viewsets

from app.models import Product
from app.serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all().order_by("ultima_actualizacion")
    serializer_class = ProductSerializer
