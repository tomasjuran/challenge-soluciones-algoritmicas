from rest_framework import serializers

from app.models import Product


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'precio_compra', 'precio_venta_publico', 'descripcion',
                  'ultima_actualizacion']
