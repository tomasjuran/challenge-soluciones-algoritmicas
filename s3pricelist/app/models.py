from django.db import models


class Product(models.Model):
    id = models.TextField(primary_key=True)
    precio_compra = models.FloatField()
    precio_venta_publico = models.FloatField()
    descripcion = models.TextField()
    ultima_actualizacion = models.DateTimeField()
